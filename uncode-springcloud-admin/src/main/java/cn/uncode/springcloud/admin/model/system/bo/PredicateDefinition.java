package cn.uncode.springcloud.admin.model.system.bo;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import lombok.Data;

/**
 * 路由断言定义模型
 */
@Data
public class PredicateDefinition implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 8040935675566099185L;

	/**
     * 断言对应的Name
     */
    private String name;

    /**
     * 配置的断言规则
     */
    private Map<String, String> args = new LinkedHashMap<>();

}
