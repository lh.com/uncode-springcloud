package cn.uncode.springcloud.starter.security.auth;

import java.io.Serializable;
import java.util.Map;

import cn.uncode.springcloud.utils.Utils;
import lombok.Data;

@Data
public class EmbedToken implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public final static String HEADER_VERSION = "headerVersion";
	public final static String APPLICATION_CODE = "applicationCode";
	public final static String CLIENT_ID = "clientId";
	public final static String SOURCE_ID = "sourceId";
	public final static String EX_SOURCE_ID = "exSourceId";
	public final static String CHANNEL = "channel";
	public final static String SUB_CHANNEL = "subChannel";
	public final static String VERSION = "version";
	public final static String USER_TOKEN = "userToken";
	
	/**
	 * 协议版本<br/>
	 * V2开始使用该key，从1开始，V1版本是数字1，V2版本是数字2
	 */
	private String headerVersion;
	/**
	 * 应用编码<br/>
	 *	命名规则如下：<br/>
	 *	应用名 + "-" + 平台名。<br/>
	 *	示例：Tenement-Android。<br/>
	 *	所有应用编码见附录1（第23行）。<br/>
	 */
	private String applicationCode;
	/**
	 * 设备编号<br/>
	 * 唯一区别每一个设备，客户端产生，安卓端取IMEI值，苹果端取OpenUDID值，再根据计算规则得到，生命周期是从设备安装APP到卸载。
	 */
	private String clientId;
	/**
	 * 下载渠道号<br/>
	 * 示例：D0000000。<br/>
	 * 所有下载渠道见WIKI：<br/>
	 * http://wiki.cjbnb.com/doku.php?id=channel:appchannel
	 */
	private String sourceId;
	/**
	 * 推广渠道号<br/>
	 * 推广渠道定义:通过渠道代码生成二维码/链接等方式来做推广的渠道,被定义成推广渠道。<br/>
	 * 示例:BTXXXX-华住  (默认)。
	 */
	private String exSourceId;
	/**
	 * 销售渠道/主渠道号<br/>
	 * 销售渠道定义:把城家房源放在第三方渠道上售卖，这类第三方渠道我们称之为销售渠道.<br/>
	 * 示例:BTXXXX-艺龙  (默认:)。
	 */
	private String channel;
	/**
	 * 销售子渠道/子渠道号<br/>
	 * H5/Web/Android/IOS/OP
	 */
	private String subChannel;
	/**
	 * 客户端版本号
	 */
	private String version;
	/**
	 * 用户令牌<br/>
	 * 登陆成功后返回。
	 */
	private String userToken;
	
	public EmbedToken() {}
	
	public EmbedToken(Map<String, Object> claims) {
		headerVersion = Utils.toStr(claims.get(HEADER_VERSION));
		applicationCode = Utils.toStr(claims.get(APPLICATION_CODE));
		clientId = Utils.toStr(claims.get(CLIENT_ID));
		sourceId = Utils.toStr(claims.get(SOURCE_ID));
		exSourceId = Utils.toStr(claims.get(EX_SOURCE_ID));
		channel = Utils.toStr(claims.get(CHANNEL));
		subChannel = Utils.toStr(claims.get(SUB_CHANNEL));
		version = Utils.toStr(claims.get(VERSION));
		userToken = Utils.toStr(claims.get(USER_TOKEN));
	}
	

}
